---
title: "About"
date: 2020-05-15T00:00:00
menu: "main"
weight: 50
---

# About
Hello there, my name is Binder News.

This is a daily blog about my adventures in self-edification. In other words, I'm recording the information I find,
things I learn, websites I visit, and various ideas and discoveries. Join me in my wanderings and queries around
the internet.
