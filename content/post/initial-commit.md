---
title: "Initial Commit"
date: 2020-05-14
categories: ["weekly"]
draft: false
---

# About
This is the beginning of what I hope to be a daily blog. I'll be recording things I've learned each day, as well
as links to various sites to help record my wanderings and queries around the internet, as well as various ideas,
discoveries, etc.
