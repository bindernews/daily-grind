---
title: My REAPER Setup
date: 2020-07-04
tags: ["reaper"]
draft: false
---

I use the [REAPER][reaper] DAW, and I love it. It's customizable in the extreme, and I can tinker with it
to make it work exactly the way I want. I'm not sure it's improved my music abilities, but it's definitely
changed the way I *want* to work with my DAW.


## Extensions and REAPER-specific plugins
Extensions are integrated into REAPER to add functionality. They are generally written in C++, and must
be available for the platform you're using. The most important two are SWS-extension and ReaPack. Personally
I would say you haven't finished installing REAPER until you've installed SWS and ReaPack.

* [SWS-extension][sws-ext] - SWS adds a bunch of mission actions to REAPER, and some extra capabilities
  for scripts. Many advanced scripts require the SWS extension to function. Generally a must-have extension.
* [ReaPack][reapack] - ReaPack is a package manager for REAPER. It makes it easy to install extra scripts,
  extensions, tools, etc. I consider this a must-have extension.
* [ReaLearn][realearn] - Advanced MIDI functionality, including feedback. This is technically a VST plugin,
  but it uses REAPER's API, so it only works in REAPER. It allows advanced MIDI communication and feedback
  so controllers like the Novation Launchpad can be controlled in REAPER.
* [Playtime][playtime] - Do you wish REAPER had a live mode like Ableton Live or FL Studio's performance mode?
  Well now it does, courtesy of Playtime. This is currently **NOT FREE**, but it's relatively cheap and it
  looks like it might eventually become open-source and free.


## Scripts
This is a list of REAPER scripts (not extensions) that I use, or like, or whatever. Some of these are links
to pages of recommendations, but I'm mostly going for tools I like. Note that items with *ReaPack* in the
title are available on [ReaPack][reapack].

* LFO tool (ReaPack) - This handy tool makes it easy to generate LFO envelopes. I used to like how FL Studio
  handled envelopes, with its built-in LFO envelope types. REAPER does have the ability to create LFO
  envelopes as well, but they break if you try to resize them, and in general I find LFO tool to be easier
  to use. One issue is that LFO does **NOT** create an undo point before modifying the envelope. This is on
  purpose, because creating undo points for every modification would be horrify inefficient, but it is
  mildly annoying. &nbsp; &nbsp;
  [LINK](https://forum.cockos.com/showthread.php?t=177437)
* ReaRack Modular Synth (ReaPack) - This is a group of JSFX plugins for things like envelope generation,
  MIDI manipulation, and some simple but powerful synthesizers. The envelope generator is REALLY cool,
  and can be matched to note-on and note-off messages, enabling envelope manipulation on a per-note basis,
  instead of modulating the entire track.
  [LINK](https://forum.cockos.com/showthread.php?t=208898)


## Miscellaneous
* [This blog post](https://reaperblog.net/2018/02/reascript-showcase/) has a nice list of great scripts,
  as well as an interview with X-Raym, who created many of the most popular scripts and tools available
  right now.


[reaper]: https://www.reaper.fm/
[reapack]: https://reapack.com/
[realearn]: https://github.com/helgoboss/realearn
[playtime]: https://www.helgoboss.org/projects/playtime/
[sws-ext]: https://www.sws-extension.org/