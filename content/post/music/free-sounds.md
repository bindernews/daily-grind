---
title: "Free Samples, Plugins, and Tools"
date: 2020-05-14T18:39:51-04:00
tags: ["vst", "music", "free", "samples"]
categories: ["music"]
draft: false
---

This is a list of sources for obtaining free samples, VST plugins, and other such audio-related things.

# Samples
* [SampleFocus](https://samplefocus.com/) is a good source of free audio samples. You get credits
  for uploading samples, and use those credits to download samples, thus perpetuating the sample ecosystem.
* [Citizen DJ](https://citizen-dj.labs.loc.gov/) a project run by the Library of Congress
* [Piano Book](https://www.pianobook.co.uk/) is a project where people submit high-quality samples of various
  instruments, usually stringed and wind instruments, but sometimes more unique sounds.
* [Free drum kit by JamesPeters](https://forum.cockos.com/showthread.php?t=233402) this guy made a free drum kit. It's
  specifically designed for REAPER but it should work with any DAW. This requires DrumGizmo (linked below).

# Plugins (VST, AU, etc.)
* [Woolyss](https://woolyss.com/chipmusic-plugins.php) has a list of free VST plugins for 8-bit music
* [mu-station's FAMISYNTH-II](http://mu-station.chillout.jp/) is a FANTASTIC plugin for 8-bit music.
  This site has the updated 64-bit version (the 32-bit version is available on Woolyss) as well as a version
  of the plugin for Mac OS.
* [delamancha](https://delamanchavst.wordpress.com/instruments/) contains, among other things, basic64 and
  basic65 which are Commodore 64 sound emulators. basic65 is the updated version. There are several other
  free instruments and effects on this site.
* [TAL-Filter-2](https://tal-software.com/products/tal-filter) is an interesting effect which I know is popular
  with other producers. It has lots of flexibility and can be used to create cool effects. 
* [Spitfire Audio LABS](https://www.spitfireaudio.com/labs) is a collection of free samples and plugins by the
  wonderful team at Spitifre Audio. They have lots of good paid stuff as well, but this is a list of free stuff.
* [Freakshow Industries](https://freakshowindustries.com/) has several _interesting_ plugins. You may purchase them,
  but you can also choose to "steal" them, in which case they'll send you a license key for free, while making
  you feel bad at the same time. =)
* [Melda Production](https://www.meldaproduction.com/downloads) has a ton of free effect plugins (reverb, pitch correction, etc.).
  They have nice UIs, and are all free. They also have paid stuff as well. I've been trying out some of their
  plugins and they work quite well.
* [u-he](https://u-he.com/products/) makes some very nice plugins both free, and paid. I've used a few before
  and they're quite nice.
* [DrumGizmo](https://drumgizmo.org/wiki/doku.php?id=getting_drumgizmo) is a sampler for drum packs. It's used by
  the drum kit from JamesPeters (linked above).
* [Sitala](https://decomposer.de/sitala/) is a drum sampler as well, but it's exactly what I want out of a drum
  kit, and is designed to work out of the box with most drum pads. I'm using it in a new project, and I quite like it.
* [Valhalla DSP](https://valhalladsp.com/) - These guys have some really nice plugins, both free and paid.
  I particularly like Valhalla Super Massive.

# Untested Plugins
I haven't tested these plugins and I cannot vouch for their quality. This is simply a list of free plugins that I
may come back to explore at a later time.

* [iZotope](https://www.izotope.com/en/products/free-audio-plug-ins.html) has a catalog of high-quality and popular
  paid plugins. Their free ones are on this page and include **Vinyl**, **Ozone Imager**, **Vocal Doubler**,
  and **Visual Mixer**.
* [Xfer Records](https://xferrecords.com/freeware) makes the incredibly popular **Serum** instrument as well as
  **LFO Tool**. This is a list of free plugins they created.
* [YouLean Loudness Meter 2](https://youlean.co/youlean-loudness-meter/) shows you the loudness of your song.
  This could be useful for mastering.
* [TDR Nova](https://www.tokyodawn.net/tdr-nova/) is a nice-looking dynamic equalizer. If your DAW doesn't have
  an EQ plugin, or you want one that works across multiple DAWs, or you just want to try a different EQ plugin,
  maybe give this a shot.
* [LoudMax](https://loudmax.blogspot.com/) is a gain plugin. Nothing else to say.

# Other Tools
* [REAPER](https://www.reaper.fm/) is a Digital Audio Workstation (DAW). It's free for 60 days, but will
  continue to allow you to use the software even after the trial period expires. Although FL Studio is
  a more popular DAW, the trial version doesn't allow you to load your projects in, meaning you can't
  really build a whole project with it unless you never close FL Studio.
* [VB-Cable](https://www.vb-audio.com/Cable/) a loop-back audio interface for Windows and Mac. You can
  use this as an audio output, and other programs (e.g. Discord, Zoom, Audacity, etc.) can read
  that output from the corresponding virtual input. I actually had issues with the Hi-Fi Cable, but
  found the regular one works just fine.

# Plugins Without Comments
This is a dump of links to random sites that have plugins. I haven't tested them, and some of the sites
look kinda sketchy. Mostly here for posterity.

* http://magnus.smartelectronix.com/
* https://illformed.org/
