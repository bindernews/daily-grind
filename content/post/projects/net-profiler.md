---
title: "Implementing a .NET Profiler"
date: 2020-05-22T21:10:36-04:00
tags: ["dotnet"]
categories: ["programming", "projects"]
draft: true
---

As part of my masters degree, I am implementing a .NET profiler program, which will, instead of obtaining
profiling data, actually use the information to attempt to detect malware. Or at least that's the idea.
Here's a link dump so I can close this Chrome Window.

## Links
* [MiniProfiler](https://github.com/MiniProfiler/dotnet) - Example profiler
* [clrprofiler](https://github.com/microsoftarchive/clrprofiler) - Example profiler from Microsoft, includes example for re-writing IL
* [Creating a Custom .NET Profiler](https://www.codeproject.com/Articles/15410/Creating-a-Custom-NET-Profiler) - Codeproject
  article about the subject, not updated recently, but the basics still apply
* [Microsoft Github search](https://github.com/microsoft?utf8=%E2%9C%93&q=profile&type=&language=) - This search probably includes
  some relevant stuff, not sure.
* [clr-samples](https://github.com/microsoft/clr-samples) - Microsoft Github repo that has examples for using the Profiling API
* [RoslynClrHeapAllocationAnalyzer](https://github.com/microsoft/RoslynClrHeapAllocationAnalyzer) - Fully-working CLR profiler
  that helps with heap analysis. Could be useful as an example.
* [jimschubert/clr-profiler](https://github.com/jimschubert/clr-profiler/tree/master/src/CLRProfiler45Source/profilerOBJ) -
  ANOTHER example profiler. These are actually quite difficult to debug and get working.
* [CorCLRProfiler](https://github.com/david-mitchell/CoreCLRProfiler) - A .NET profiler that works with .NET Core, at least
  on OS X, maybe on Linux
* [Registry-Free Profiler Startup and Attach](https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-4.0/ee471451%28v%3dvs.100%29) -
  Microsoft docs on how to attach a profiler to a process when it starts up
* [Setting up a Profiling Environment](https://docs.microsoft.com/en-us/dotnet/framework/unmanaged-api/profiling/setting-up-a-profiling-environment) -
  More Microsoft docs
* [.NET Core runtime source code](https://github.com/dotnet/runtime) - useful for debugging
* [Debugging your Profiler](https://docs.microsoft.com/en-us/archive/blogs/davbr/debugging-your-profiler-i-activation) -
  OLD tutorial on debugging a .NET profiler
* [ICorProfilerCallback5 API](https://docs.microsoft.com/en-us/dotnet/framework/unmanaged-api/profiling/icorprofilercallback5-conditionalweaktableelementreferences-method)
* [Using GetFunctionInfo to get the name of a function from a FunctionID](https://stackoverflow.com/questions/47340776/using-getfunctioninfo-to-get-the-name-of-a-function-from-a-functionid)
* [.NET Profiling API Initialization](https://stackoverflow.com/questions/55958910/net-profiling-api-initialization)
* [Monitoring and Observability in the .NET Runtime](https://mattwarren.org/2018/08/21/Monitoring-and-Observability-in-the-.NET-Runtime/)
