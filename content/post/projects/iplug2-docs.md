---
title: "Dump about iPlug2"
date: 2020-06-25T12:00:00-04:00
tags: ["iplug2", "cbor"]
draft: false
---

# General
This is a dump of information about [iPlug2](https://github.com/iPlug2/iPlug2) which is a framework for building
audio plugins and instruments. It supports building VST2, VST3, AU, AUv3, standalone apps, WAM, and a few other
formats as well. It includes a built-in graphics library. Really fantastic work. That being said, it's still
beta software, and the documentation is somewhat lacking. Here I'll contribute some of my own miscellaneous
documentation as I learn things.

# Saving and Loading State: CBOR
Some people like to write their own serialization format, or they use JSON or XML. Using JSON isn't terrible,
but it doesn't really work well for lots of binary data. XML is almost never the correct choice for any situation,
I'd always recommend JSON over XML. YAML is okay, but it's hard to parse and more designed for human-readability
than saving and loading state.

I also don't really like building custom serialization formats, because then backwards-compatibility usually
becomes an issue, and it's often nearly impossible to detect errors when using a custom serialization format,
except that if a length value is invalid you'll end up reading outside an array, which will cause an error eventually.

Instead of those things, I recommend CBOR, Conscise Binary Object Representation. It's somewhat similar to JSON
or BSON, but it doesn't have the extra weirdness that BSON has, and it's binary unlike JSON. There are decoders
and encoders in [pretty much every language][cbor-impls], and if there isn't one for your language, the spec is 
easy to implement. CBOR doesn't have much in the way of error-checking capabilities, but it would be relatively
easy to add a CRC somewhere to verify data integrity. It's basically what I would design for a general-purpose
serialization format.


# Compiler Defines
Many things in iPlug2 are controlled by compiler defines. Some of these are obvious, some are basically undocumented.
* **VST3_NUM_CC_CHANS=0**: By default iPlug2 will include a parameter for every MIDI channel when you use the VST3
  output. This shows up in your DAW, but it doesn't really help you unless you are actually responding to those
  MIDI messages. To use this flag to disable these extra parameters.
* **VST3_PRESET_LIST**: By default VST3 plugins don't support built-in preset lists like VST2 does. I'm not
  really sure why, but how it is. However if you define this flag, your built-in presets will show up in your DAW.
  [SOURCE](https://www.kvraudio.com/forum/viewtopic.php?t=535327)
* **IGRAPHICS_CPU**: This is only available when using **IGRAPHICS_SKIA** but it reduces memory usage significantly,
  at least for me. Generally for iPlug2 GPU-based backends are a bit faster, but consume a lot more memory. Skia
  is the recommended graphics backend, and using its CPU mode is plenty fast and not very memory intensive.

[cbor-impls]: https://cbor.io/impls.html
