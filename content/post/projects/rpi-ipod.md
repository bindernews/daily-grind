---
title: "Making a Raspberry Pi into a fake iPod"
date: 2020-06-11T16:20:37-04:00
tags: ["ipod", "raspberry pi", "car", "bluetooth", "audio"]
categories: ["projects"]
draft: false
---

I have an older Honda CR-V that doesn't have Bluetooth built-in. At the moment, I have a 
[USB-C and AUX splitter](https://www.amazon.com/Compatible-Dreamvasion-Headphone-Charging-Converter/dp/B07FDT1SGG)
so I can charge my phone and play music out of it at the same time. Unfortuantely, this setup is a bit janky,
and the cord seems to come slightly loose constantly. It could be the splitter, it could be something else,
but it's quite annoying. Furthermore my phone isn't getting any sound in, meaning I have to unplug my phone
to make or receive calls, which is dangerous, so I rarely do so. No hands-free driving for me! One more downside
to my current setup is that I can't change the song using the car wheel. It's a minor nitpick, but if I'm
going to try to solve this, I'm going all the way.

Now what my car DOES have is an iPod-compatible USB port, from back when those were still popular. If I can
build a device that will ACT like an iPod, I can play music out of that USB port. If that device can then
talk to my phone via Bluetooth, problem solved! Next question: what would this magical device be?

A [Raspberry Pi Zero W](https://www.raspberrypi.org/products/raspberry-pi-zero-w/) of course! It comes
with built-in WiFi and Bluetooth, meaning I don't have to purchase exta things to handle that, and it can
act as a USB device, which should allow me to make it pretend to be an iPod. Furthermore I'm pretty sure
I can power it from the USB port, which would be perfect.

There's also the [Samson 30-Pin Bluetooth Receiver](https://www.amazon.com/Samson-30-Pin-Bluetooth-Receiver-SABT30/dp/B00J0Q6PMC)
which does pretty much exactly what I want, but has two major downsides. First, I don't think I can get
audio input from this thing, which might be a problem, and might not. Second, I don't get to mess around
with an RPi Zero W, which is definitely more fun.

Currently the plan is to see if the Samson device works well enough. If I can't get sound input, that's unfortunate
but it's still much better than what I have right now. Ideally I'd like to use the RPi Zero W, and have a dedicated
microphone in the car, so I can just speak and have it pick up my voice. Basically a DIY Bluetooth system. But 
that kind of project is for when I have more time.

Links:
* [Arduino forum post about a similar question](https://forum.arduino.cc/index.php?topic=283838.0)
* [Arduino Sketch to talk to an iPod](https://github.com/finsprings/arduinaap)
* [Apple Accessory Protocol documentation](http://www.ipodlinux.org/Apple_Accessory_Protocol/)
* [Turning an RPi Zero into a USB Gadget](https://learn.adafruit.com/turning-your-raspberry-pi-zero-into-a-usb-gadget/serial-gadget)
* [Cheap mic for cars](https://www.amazon.com/FLTP-Microphone-Vehicle-Bluetooth-Enabled/dp/B019KPN9I0)
* [Audio Streaming with Raspberry Pi Zero W and Adafruit’s I2S Mic](http://www.heidislab.com/tutorials/audio-streaming-with-raspberry-pi-zero-w-and-adafruits-i2s-mic)
* https://www.arduino.cc/en/Reference/I2S
* [Adafruit I2S Stereo Decoder - UDA1334A Breakout](https://www.adafruit.com/product/3678?gclid=CjwKCAjwxt_tBRAXEiwAENY8hZYnnmaDm0vO8-VcDhQwgQ1yTnLQu70zA6FCGsMgWoJ6_6oMFUlcixoCqToQAvD_BwE)
* [ipod-gadget](https://github.com/oandrew/ipod-gadget)
* [Controlling an Apple iPod with the Atmel Mega32](https://people.ece.cornell.edu/land/courses/ece4760/FinalProjects/s2007/awr8_asl45/awr8_asl45/index.html)
